"""Main entrypoint for the hivle sensor device"""

import logging
from time import sleep

import board
from digitalio import DigitalInOut

from .client.client import MQTTClient
from .client.configuration import MQTTConfig
from .definitions import CONFIG_FILE
from .motion_sensor.motion_sensor import MotionSensor, Edge


def main() -> None:
    """Main entrypoint"""
    logging.basicConfig(level=logging.INFO)

    motion_sensor = MotionSensor(DigitalInOut(board.D18))

    config = MQTTConfig.load(CONFIG_FILE)
    client = MQTTClient(config)

    client.connect()
    client.loop_start()

    # Necessary, paho client advise
    sleep(1)

    while True:
        try:
            motion_sensor.read()
            if motion_sensor.analyze_edge(Edge.RISING):
                client.publish("hivle/motion-sensor/movement", "Motion detected")

            sleep(0.5)
        except KeyboardInterrupt:
            break


if __name__ == "__main__":
    main()
