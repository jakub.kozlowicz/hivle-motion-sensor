"""MQTT Client"""

import logging
from typing import Any

import paho.mqtt.client as mqtt

from .configuration import MQTTConfig


class MQTTClient:
    """Client for the MQTT protocol."""

    def __init__(self, config: MQTTConfig) -> None:
        self._config: MQTTConfig = config
        self._client: mqtt.Client = mqtt.Client("HivleMotionSensor", reconnect_on_failure=True)
        self._client.username_pw_set(username=self._config.user, password=self._config.password)

    def connect(self) -> None:
        """Connect to MQTT broker."""

        def on_connect(
            client: mqtt.Client, user_data: Any, flags: dict[str, Any], return_code: int
        ) -> None:
            del client, user_data, flags

            if return_code != 0:
                msg = f"Failed to connect to MQTT broker -- return code {return_code}"
                logging.error(msg)
                raise RuntimeError(msg)

            logging.info("Connected to MQTT broker")

        self._client.on_connect = on_connect
        self._client.connect(self._config.address, self._config.port, keepalive=30)

        self._client.on_message = self.on_message

    def subscribe(self, topic: str) -> None:
        """
        Subscribe to given topic.

        :param topic: Topic name to subscribe in broker.
        :type topic: str
        """
        self._client.subscribe(topic)
        logging.info("Subscribed to topic %s", topic)

    def publish(self, topic: str, payload: str) -> None:
        """
        Publish a message with given payload to specified topic.

        :param topic: Topic name
        :type topic: str
        :param payload: Message payload
        :type payload: str
        """
        self._client.publish(topic, payload)
        logging.info("Published message on topic %s with payload %s", topic, payload)

    def register_topics(self, topics: list[str]) -> None:
        """
        Register multiple topics to subscribe.

        :param topics: List with topics
        :type topics: list[str]
        """
        for topic in topics:
            self.subscribe(topic)

    def on_message(self, client: mqtt.Client, user_data: Any, msg: mqtt.MQTTMessage) -> None:
        """
        Callback to run when new message arrived to broker and this client receive it.

        :param client: Client handle
        :type client: mqtt.Client
        :param user_data: Some user data
        :type user_data: Any
        :param msg: MQTT message sent to the broker
        :type msg: mqtt.MQTTMessage
        """
        del client, user_data
        logging.info("Get message on topic %s with payload %s", msg.topic, msg.payload.decode())

        if msg.topic == "hivle/motion-sensor/config":
            pass

    def loop_start(self) -> None:
        """Start networking loop on separate thread."""
        self._client.loop_start()
