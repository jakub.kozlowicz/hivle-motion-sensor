"""Client configuration"""

from __future__ import annotations

from dataclasses import dataclass
import json
from pathlib import Path


@dataclass
class MQTTConfig:
    """MQTT Client configuration."""

    user: str
    password: str
    address: str
    port: int

    @classmethod
    def load(cls, config_file: Path) -> MQTTConfig:
        """Reads configuration from file."""
        config = json.loads(config_file.read_text())
        return cls(**config)
