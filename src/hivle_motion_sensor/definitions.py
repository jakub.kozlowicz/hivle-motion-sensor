"""Constant variables to use across package."""

from pathlib import Path


CONFIG_FILE = Path("/etc/hivle/config.json")
