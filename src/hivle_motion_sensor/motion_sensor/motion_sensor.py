"""Motion sensor device representation."""

from enum import Flag, auto

from digitalio import DigitalInOut, Pull


class Edge(Flag):
    """Represents edge on GPIO pin."""

    FALLING = auto()
    RISING = auto()


class MotionSensor:
    """Motion sensor device"""

    def __init__(self, pin: DigitalInOut) -> None:
        self.pin = pin
        self.pin.switch_to_input(pull=Pull.UP)

        self._current_value = 0
        self._status_value = 0
        self._lock_value = 0

    def read(self) -> None:
        """Reads value of the motion sensor."""
        self._current_value = int(self.pin.value)

    def analyze_edge(self, edge: Edge) -> bool:
        """Analyze if edge occurred."""
        if self._current_value == 1:
            if self._lock_value == 1 and self._status_value == 0:
                self._status_value = 1
                if edge == Edge.RISING:
                    return True
            self._lock_value = min(1, self._status_value + 1)
        else:
            if self._lock_value == -1 and self._status_value == 1:
                self._status_value = 0
                if edge == Edge.FALLING:
                    return True
            self._lock_value = max(-1, self._lock_value - 1)

        return False
