"""Unit tests for definitions module."""

from unittest import TestCase
from pathlib import Path

from hivle_motion_sensor.definitions import CONFIG_FILE


class TestDefinitions(TestCase):
    """Test class for definitions module"""

    def test_config_file(self) -> None:
        """Tests CONFIG_FILE path"""
        self.assertEqual(CONFIG_FILE, Path("/etc/hivle/config.json"))
